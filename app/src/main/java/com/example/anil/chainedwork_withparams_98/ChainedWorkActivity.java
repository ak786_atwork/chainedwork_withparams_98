package com.example.anil.chainedwork_withparams_98;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

public class ChainedWorkActivity extends AppCompatActivity {

    OneTimeWorkRequest workA, workB, workC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chained_work);

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkManager.getInstance()
                        // First, run all the A tasks (in parallel):
                        .beginWith(startWorkA())
                        // ...when all A tasks are finished, run the single B task:
                        .then(startWorkB())
                        // ...then run the C tasks (in any order):
                        .then(startWorkC())
                        .enqueue();
            }
        });

    }

    public Data createData(String info)
    {
        Data data = new Data.Builder()
                .putString("msg", info)
                .build();
        return data;
    }

    public OneTimeWorkRequest startWorkA()
    {
        workA = new OneTimeWorkRequest.Builder(SimpleWork.class)
                .setInputData(createData("work A started"))
                .build();
        return workA;
    }
    public OneTimeWorkRequest startWorkB()
    {
        workB = new OneTimeWorkRequest.Builder(SimpleWork.class)
                .setInputData(createData("work B started"))
                .build();
        return workB;
    }
    public OneTimeWorkRequest startWorkC()
    {
        workC= new OneTimeWorkRequest.Builder(SimpleWork.class)
                .setInputData(createData("work C started"))
                .build();
        return workC;
    }



}
