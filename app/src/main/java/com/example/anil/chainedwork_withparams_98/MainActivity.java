package com.example.anil.chainedwork_withparams_98;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import static com.example.anil.chainedwork_withparams_98.MathWorker.KEY_RESULT;
import static com.example.anil.chainedwork_withparams_98.MathWorker.KEY_X_ARG;
import static com.example.anil.chainedwork_withparams_98.MathWorker.KEY_Y_ARG;
import static com.example.anil.chainedwork_withparams_98.MathWorker.KEY_Z_ARG;

public class MainActivity extends AppCompatActivity {

    OneTimeWorkRequest mathWork;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create the Data object:
        Data myData = new Data.Builder()
                // We need to pass three integers: X, Y, and Z
                .putInt(KEY_X_ARG, 42)
                .putInt(KEY_Y_ARG, 421)
                .putInt(KEY_Z_ARG, 8675309)
                // ... and build the actual Data object:
                .build();

// ...then create and enqueue a OneTimeWorkRequest that uses those arguments
        mathWork = new OneTimeWorkRequest.Builder(MathWorker.class)
                .setInputData(myData)
                .build();
        WorkManager.getInstance().enqueue(mathWork);

        textView = findViewById(R.id.status);
        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getResult();
            }
        });


    }


    public void getResult() {

        WorkManager.getInstance().getWorkInfoByIdLiveData(mathWork.getId())
                .observe(MainActivity.this, new Observer<WorkInfo>() {
                    @Override
                    public void onChanged(@Nullable WorkInfo workInfo) {
                        if (workInfo != null && workInfo.getState().isFinished()) {
                            int  result = workInfo.getOutputData().getInt(KEY_RESULT, -1);
                            // ... do something with the result ...
                            textView.setText(" " +result );
                        }
                    }
                });
    }
}
