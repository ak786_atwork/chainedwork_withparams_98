package com.example.anil.chainedwork_withparams_98;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class SimpleWork extends Worker {

    public SimpleWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        String msg = getInputData().getString("msg");
        showLog(msg);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // Indicate success or failure with your return value:
        return Result.SUCCESS;

    }

    public void showLog(String text)
    {
        Log.d("Chained work " , text);
        //Toast.makeText(this,text,Toast.LENGTH_SHORT).show();
    }
}
